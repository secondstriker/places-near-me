package com.codewithmohsen.placesnearme.db

import com.codewithmohsen.placesnearme.model.venues.local.Category
import com.codewithmohsen.placesnearme.model.venues.local.Icon
import com.codewithmohsen.placesnearme.model.venues.local.Location
import com.codewithmohsen.placesnearme.model.venues.local.Venue


object TestUtil {

    fun createVenue(): Venue {

        return Venue(3,"venueId04", "venueName",
            createLocation(), mutableListOf(createCategory()))
    }

    fun createLocation(): Location{

        return Location(
            "tehran",
            null,
            1.0, 2.0,
            null,
        5, null, null, null, null, null, "15")
    }

    fun createCategory(): Category{

        return Category(1,
            "categoryId01",
        "name",
        "pluralName",
        "shortName",
            createIcon(),
            null,
            "venueIdCategory02")
    }

    fun createIcon(): Icon{

        return Icon("prefix", "suffix")
    }

}
