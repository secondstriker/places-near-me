package com.codewithmohsen.placesnearme.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.codewithmohsen.placesnearme.model.venues.local.VenueWithCategories
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert
import org.junit.Test

import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AppDbTest: AbstractDbTest() {

    @Test
    fun getVenueDao() {
    }

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun insertAndRead() {
        val venue = TestUtil.createVenue()
        val category = TestUtil.createCategory()
        val icon = TestUtil.createIcon()

        runBlocking {
            db.getVenueDao().insertAll(
                mutableListOf(VenueWithCategories(venue, mutableListOf(category))))

            val loaded = db.getVenueDao().loadVenues()

            MatcherAssert.assertThat(loaded, notNullValue())
            MatcherAssert.assertThat(loaded.count(), `is`(1))
            MatcherAssert.assertThat(loaded[0].venue.name, `is`(venue.name))
            MatcherAssert.assertThat(loaded[0].venue.location, `is`(venue.location))
            MatcherAssert.assertThat(loaded[0].categories, notNullValue())
            MatcherAssert.assertThat(loaded[0].categories?.count(), `is`(1))
            MatcherAssert.assertThat(loaded[0].categories?.get(0)?.icon, `is`(icon))
            MatcherAssert.assertThat(loaded[0].categories?.get(0)?.categoryId, `is`(category.categoryId))
            MatcherAssert.assertThat(loaded[0].categories?.get(0)?.relatedVenueId, `is`(venue.venueId))

        }
    }

    @Test
    fun insertAndDelete() {
        val venue = TestUtil.createVenue()
        val category = TestUtil.createCategory()

        runBlocking {
            db.getVenueDao().insertAll(
                mutableListOf(VenueWithCategories(venue, mutableListOf(category))))
            db.getVenueDao().deleteAll()
            val loaded = db.getVenueDao().loadVenues()

            MatcherAssert.assertThat(loaded, notNullValue())
            MatcherAssert.assertThat(loaded.count(), `is`(0))
        }
    }
}