package com.codewithmohsen.placesnearme.utils

import com.codewithmohsen.placesnearme.model.venues.local.UserLocation
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ExtensionsKtDistanceTest {


    @Test
    fun `distance when called on two same points should return 0 meter`() {
        val pointA = UserLocation(1.0, 1.0)
        val pointB = UserLocation(1.0, 1.0)

        assertEquals(0.0, pointA.distance(pointB), 0.0)
    }

    @Test
    fun `distance in about 100 meters should return the true distance with maximum 2 meters error`() {
        //46.13499741361976, 1.8411730237302464
        val pointA = UserLocation(46.13499741361976, 1.8411730237302464)
        //46.13520558401304, 1.8424604840976841 distance 99.93 meters calculated by google map
        val pointB = UserLocation(46.13520558401304, 1.8424604840976841)

        assertEquals(99.93, pointA.distance(pointB), 2.0)
    }

    @Test
    fun `distance in about 1000 meters should return the true distance with maximum 2 meters error`() {
        //46.13499741361976, 1.8411730237302464
        val pointA = UserLocation(46.13499741361976, 1.8411730237302464)
        //46.1360233886542, 1.8541978311141551 distance 1.01 km calculated by google map
        val pointB = UserLocation(46.1360233886542, 1.8541978311141551)

        assertEquals(1010.0, pointA.distance(pointB), 2.0)
    }

    @Test
    fun `distance in about 5000 meters should return the true distance with maximum 2 meters error`() {
        //46.13499741361976, 1.8411730237302464
        val pointA = UserLocation(46.13499741361976, 1.8411730237302464)
        //46.1386014890904, 1.771918990929162 distance 5.35 km calculated by google map
        val pointB = UserLocation(46.1386014890904, 1.771918990929162)

        assertEquals(5350.0, pointA.distance(pointB), 2.0)
    }
}