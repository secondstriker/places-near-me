package com.codewithmohsen.placesnearme.api

import com.codewithmohsen.placesnearme.model.details.DetailsResponse
import com.codewithmohsen.placesnearme.model.venues.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("venues/explore")
    suspend fun getNearestVenues(
        @Query("ll") location: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("sortByDistance") sort: Int = 1

    ): NetworkResponse<ApiGeneralResponse<Response>, Any>


    @GET("venues/{venue_id}")
    suspend fun getDetails(
        @Path(value = "venue_id")
        venueId: String
    )
    : NetworkResponse<ApiGeneralResponse<DetailsResponse>, Any>
}