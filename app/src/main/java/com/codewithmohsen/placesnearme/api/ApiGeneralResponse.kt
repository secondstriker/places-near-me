package com.codewithmohsen.placesnearme.api


class ApiGeneralResponse<T>(
    val meta: Meta,
    val response: T?
)
