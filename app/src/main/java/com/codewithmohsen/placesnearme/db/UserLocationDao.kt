package com.codewithmohsen.placesnearme.db

import androidx.room.*
import com.codewithmohsen.placesnearme.model.venues.local.Category
import com.codewithmohsen.placesnearme.model.venues.local.UserLocation
import com.codewithmohsen.placesnearme.model.venues.local.Venue
import com.codewithmohsen.placesnearme.model.venues.local.VenueWithCategories


@Dao
abstract class UserLocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(userLocation: UserLocation)

    @Query("DELETE FROM user_location_table")
    abstract fun delete()

    @Query("SELECT * FROM user_location_table WHERE id = 0")
    abstract fun getUserLocation(): UserLocation?
}