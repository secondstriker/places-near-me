package com.codewithmohsen.placesnearme.db

import androidx.room.*
import com.codewithmohsen.placesnearme.model.venues.local.Category
import com.codewithmohsen.placesnearme.model.venues.local.Venue
import com.codewithmohsen.placesnearme.model.venues.local.VenueWithCategories


@Dao
abstract class VenueDao {

    //need to change add related venue id
    // for every single category to make a relation
    private suspend fun insertCategoriesOfVenues(categories: List<Category>, venueId: String){
        categories.forEach {
            it.relatedVenueId = venueId
        }
        _insertCategories(categories)
    }

    /**
     * Do not use this method to add venue, instead
     * use insertAll function.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun _insertVenues(venues: List<Venue>)

    /**
     * Do not use this method to add category, instead
     * use insertAll method
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun _insertCategories(categories: List<Category>)

    /**
     * Do not use this method, instead, use deleteAll
     */
    @Query("DELETE FROM venue_table")
    abstract fun _deleteAllVenues()

    /**
     * Do not use this method, instead, use deleteAll
     */
    @Query("DELETE FROM category_table")
    abstract fun _deleteAllCategories()

    /**
     * Use this method to delete everything from db
     */
    public suspend fun deleteAll(){
        _deleteAllVenues()
        _deleteAllCategories()
    }

    /**
     * Use this method to save venues and related categories on db
     */
    public suspend fun insertAll(vWCList: List<VenueWithCategories>) {
        //because we need to make category list object for every single venue,
        //we save it like this
        val venue = mutableListOf<Venue>()
        for (vWC in vWCList) {
            if (vWC.categories != null)
                insertCategoriesOfVenues(vWC.categories!!, vWC.venue.venueId)
           venue.add(vWC.venue)
        }
        _insertVenues(venue)
    }

    /**
     * Returns all the venues with related categories, sorted by the primary key
     */
    @Transaction
    @Query("SELECT * From venue_table")
    abstract suspend fun loadVenues(): List<VenueWithCategories>
}