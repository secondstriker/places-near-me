package com.codewithmohsen.placesnearme.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.codewithmohsen.placesnearme.model.venues.local.Category
import com.codewithmohsen.placesnearme.model.venues.local.UserLocation
import com.codewithmohsen.placesnearme.model.venues.local.Venue
import javax.inject.Singleton


@Singleton
@Database(entities = [Venue::class, Category::class, UserLocation::class], version = 1, exportSchema = false)
abstract class AppDb : RoomDatabase() {

    abstract fun getVenueDao(): VenueDao
    abstract fun getUserLocationDao(): UserLocationDao
}
