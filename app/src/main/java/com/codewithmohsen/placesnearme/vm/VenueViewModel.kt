package com.codewithmohsen.placesnearme.vm

import androidx.lifecycle.ViewModel
import com.codewithmohsen.placesnearme.repository.VenueRepository
import javax.inject.Inject

class VenueViewModel @Inject constructor(
    private val repo: VenueRepository): ViewModel()  {

    fun setLl(latitude: Double, longitude: Double) = repo.setLl(latitude, longitude)


    fun getMoreVenues() = repo.getMoreVenues()
    fun getVenues() = repo.getVenues()
}