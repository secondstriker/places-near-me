package com.codewithmohsen.placesnearme.vm

import androidx.lifecycle.ViewModel
import com.codewithmohsen.placesnearme.api.ApiService
import com.codewithmohsen.placesnearme.repository.DetailsRepository
import com.codewithmohsen.placesnearme.repository.VenueRepository
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val detailsRepository: DetailsRepository): ViewModel()  {

    fun getDetails(venueId: String) = detailsRepository.getVenueDetails(venueId)
}