package com.codewithmohsen.placesnearme.repository

import androidx.lifecycle.LiveData
import com.codewithmohsen.placesnearme.model.details.VenueDetails

interface DetailsRepository {

    fun getVenueDetails(venueId: String): LiveData<Resource<VenueDetails>>
}