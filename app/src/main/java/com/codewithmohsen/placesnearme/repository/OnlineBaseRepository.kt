package com.codewithmohsen.placesnearme.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.codewithmohsen.placesnearme.AppCoroutineDispatchers
import com.codewithmohsen.placesnearme.api.NetworkResponse
import com.codewithmohsen.placesnearme.utils.exhaustive
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 * A generic class that can provide a resource backed by the network.
 *
 *
 * You can read more about it in the [Architecture
 * Guide](https://developer.android.com/arch).
 * @param <ResultType>
 * @param <RequestType>
</RequestType></ResultType> */
abstract class OnlineBaseRepository<ResultType, RequestType : Any>
constructor(private val dispatchers: AppCoroutineDispatchers){

    private val result = MutableLiveData<Resource<ResultType>>()


    fun fetchNetwork() {
        CoroutineScope(dispatchers.main).launch {

            result.value = Resource.loading(null)

            val apiResponse = createCall()
            when (apiResponse) {
                is NetworkResponse.Success -> {

                    withContext(dispatchers.main) {

                        result.value = successRequestToResult(
                            processResponse(apiResponse))
                    }
                }

                is NetworkResponse.ApiError -> {
                    withContext(dispatchers.main) {
                        onFetchFailed()
                        result.value = Resource.error("Api error", null)
                    }
                }
                is NetworkResponse.NetworkError -> {
                    withContext(dispatchers.main) {
                        onFetchFailed()
                        result.value = Resource.error(apiResponse.error.message, null)
                    }
                }
                is NetworkResponse.UnknownError -> {
                    withContext(dispatchers.main) {
                        onFetchFailed()
                        result.value = Resource.error(apiResponse.error?.message, null)
                    }
                }
            }.exhaustive
        }
    }

    open fun onFetchFailed() {}

    protected fun liveDataResult() = result as LiveData<Resource<ResultType>>

    @WorkerThread
    protected open suspend fun processResponse(response: NetworkResponse.Success<RequestType>) =
        response.body

    @MainThread
    protected abstract suspend fun createCall(): NetworkResponse<RequestType, Any>

    @MainThread
    protected abstract fun successRequestToResult(requestType: RequestType): Resource<ResultType>
}
