package com.codewithmohsen.placesnearme.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.codewithmohsen.placesnearme.AppCoroutineDispatchers
import com.codewithmohsen.placesnearme.api.ApiGeneralResponse
import com.codewithmohsen.placesnearme.api.ApiService
import com.codewithmohsen.placesnearme.api.NetworkResponse
import com.codewithmohsen.placesnearme.model.details.DetailsResponse
import com.codewithmohsen.placesnearme.model.details.VenueDetails
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsRepositoryImpl@Inject constructor(
    private val dispatchers: AppCoroutineDispatchers,
    private val service: ApiService
):OnlineBaseRepository<VenueDetails, ApiGeneralResponse<DetailsResponse>>(dispatchers), DetailsRepository {

    private lateinit var venueId: String

    override fun getVenueDetails(venueId: String): LiveData<Resource<VenueDetails>> {
        this.venueId = venueId
        CoroutineScope(dispatchers.IO).launch {
            super.fetchNetwork()
        }
        return super.liveDataResult()
    }

    override suspend fun createCall(): NetworkResponse<ApiGeneralResponse<DetailsResponse>, Any> =
        service.getDetails(venueId)

    override fun successRequestToResult(requestType: ApiGeneralResponse<DetailsResponse>): Resource<VenueDetails> =
        Resource.success(requestType.response?.venueDetails)
}