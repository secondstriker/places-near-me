package com.codewithmohsen.placesnearme.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.codewithmohsen.placesnearme.AppCoroutineDispatchers
import com.codewithmohsen.placesnearme.api.NetworkResponse
import com.codewithmohsen.placesnearme.utils.exhaustive
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 * A generic class that can provide a resource backed by the network.
 *
 *
 * You can read more about it in the [Architecture
 * Guide](https://developer.android.com/arch).
 * @param <ResultType>
 * @param <RequestType>
</RequestType></ResultType> */
abstract class OfflineFirstBaseRepository<ResultType, RequestType : Any>
constructor(private val dispatchers: AppCoroutineDispatchers) {

    private val result = MutableLiveData<Resource<ResultType>>()

     fun showDbDataAsLive(){
        CoroutineScope(dispatchers.IO).launch {
            result.postValue(Resource.success(loadFromDb()))
        }
    }

    fun fetchNetwork(deleteDbIfSuccess: Boolean = false) {
        CoroutineScope(dispatchers.main).launch {

            if(deleteDbIfSuccess)
                result.value = Resource.loading(null)
            else
                result.value = Resource.loading(result.value?.data)

            val apiResponse = createCall()
            when (apiResponse) {
                is NetworkResponse.Success -> {

                    withContext(dispatchers.IO) {
                        if (deleteDbIfSuccess)
                            deleteDb()

                        val processedResult = successRequestToResult(
                            processResponse(apiResponse))

                        saveCallResult(processedResult)
                        result.postValue(Resource.success(loadFromDb()))
                    }

                }

                is NetworkResponse.ApiError -> {
                    withContext(dispatchers.main) {
                        result.value = Resource.error("Api error", loadFromDb())
                    }
                }
                is NetworkResponse.NetworkError -> {
                    withContext(dispatchers.main) {
                        onFetchFailed()
                        result.value = Resource.error(apiResponse.error.message, loadFromDb())
                    }
                }
                is NetworkResponse.UnknownError -> {
                    withContext(dispatchers.main) {
                        onFetchFailed()
                        result.value = Resource.error(apiResponse.error?.message, loadFromDb())
                    }
                }
            }.exhaustive
        }
    }

    protected open fun onFetchFailed() {}

    protected fun liveDataResult() = result as LiveData<Resource<ResultType>>

    @WorkerThread
    protected open suspend fun processResponse(response: NetworkResponse.Success<RequestType>) =
        response.body

    @MainThread
    protected abstract suspend fun createCall(): NetworkResponse<RequestType, Any>

    @MainThread
    protected abstract fun successRequestToResult(requestType: RequestType): Resource<ResultType>

    @WorkerThread
    protected abstract suspend fun saveCallResult(item: Resource<ResultType>)

    @WorkerThread
    protected abstract suspend fun loadFromDb(): ResultType

    @WorkerThread
    protected open suspend fun deleteDb(): Int = 0

    @MainThread
    protected abstract fun shouldFetch(): Boolean
}
