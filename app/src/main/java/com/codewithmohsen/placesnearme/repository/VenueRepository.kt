package com.codewithmohsen.placesnearme.repository

import androidx.lifecycle.LiveData
import com.codewithmohsen.placesnearme.model.venues.local.VenueWithCategories

interface VenueRepository {

    fun setLl(latitude: Double, longitude: Double)
    fun getVenues(): LiveData<Resource<List<VenueWithCategories>>>
    fun getMoreVenues()
}