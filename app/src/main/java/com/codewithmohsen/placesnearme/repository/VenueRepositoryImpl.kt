package com.codewithmohsen.placesnearme.repository

import com.codewithmohsen.placesnearme.AppConstants
import com.codewithmohsen.placesnearme.AppCoroutineDispatchers
import com.codewithmohsen.placesnearme.api.ApiGeneralResponse
import com.codewithmohsen.placesnearme.api.ApiService
import com.codewithmohsen.placesnearme.api.NetworkResponse
import com.codewithmohsen.placesnearme.db.UserLocationDao
import com.codewithmohsen.placesnearme.db.VenueDao
import com.codewithmohsen.placesnearme.model.venues.Response
import com.codewithmohsen.placesnearme.model.venues.local.UserLocation
import com.codewithmohsen.placesnearme.model.venues.local.VenueWithCategories
import com.codewithmohsen.placesnearme.utils.distance
import com.codewithmohsen.placesnearme.utils.durationTimeWithLastSavedLocation
import com.codewithmohsen.placesnearme.utils.toNextPage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class VenueRepositoryImpl @Inject constructor(
    private val dispatchers: AppCoroutineDispatchers,
    private val dao: VenueDao,
    private val userLocationDao: UserLocationDao,
    private val service: ApiService
): OfflineFirstBaseRepository<List<VenueWithCategories>, ApiGeneralResponse<Response>>(dispatchers), VenueRepository {

    private var ll: String = ""
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var nextPage = 1


    override fun getMoreVenues() {
        //we need to prevent get more venues when nextPage is 1,
        //because they already there
        if(nextPage > 1)
            super.fetchNetwork(false)
    }

    override fun getVenues() = super.liveDataResult()

    override fun setLl(latitude: Double, longitude: Double){
        this.latitude = latitude
        this.longitude = longitude
        this.ll = "$latitude, $longitude"

        CoroutineScope(dispatchers.IO).launch {
            if (shouldFetch()) {
                nextPage = 1
                super.fetchNetwork(true)
            }
            else super.showDbDataAsLive()
        }
    }

    override suspend fun createCall(): NetworkResponse<ApiGeneralResponse<Response>, Any> {
        val offset = (nextPage - 1) * AppConstants.OFFSET

        return service.getNearestVenues(
            ll,
            AppConstants.OFFSET,
            offset)
    }

    override fun successRequestToResult(requestType:  ApiGeneralResponse<Response>): Resource<List<VenueWithCategories>> {
        val venues = mutableListOf<VenueWithCategories>()
        requestType.response?.groups?.get(0).let { group ->
            group?.items?.forEach {
            if(it.venue != null)
                venues.add(VenueWithCategories(it.venue, it.venue.categories))
            }
        }
        return Resource.success(venues)
    }

    override suspend fun saveCallResult(item: Resource<List<VenueWithCategories>>) {
        if(item.data != null){
            dao.insertAll(item.data)
            //here we need to save userLocation
            val userLocation = UserLocation(latitude, longitude, Date().time)
            userLocationDao.insert(userLocation)
        }
    }

    override suspend fun loadFromDb(): List<VenueWithCategories> {
        val venues = dao.loadVenues()
        venues.also { nextPage = it.toNextPage(AppConstants.OFFSET) }
        return venues
    }

    override suspend fun deleteDb(): Int {
        dao.deleteAll()
        userLocationDao.delete()
        return super.deleteDb()
    }

    override fun shouldFetch(): Boolean {
        val preUserLocation: UserLocation = userLocationDao.getUserLocation() ?: return true

        val userLocation = UserLocation(latitude, longitude, Date().time)
        val distance = userLocation.distance(preUserLocation)
        val duration = userLocation.durationTimeWithLastSavedLocation(preUserLocation)

        return distance > AppConstants.MAX_DISTANCE ||
                duration > AppConstants.MAX_ACCEPTABLE_DURATION
    }
}