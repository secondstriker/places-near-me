package com.codewithmohsen.placesnearme.di


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.codewithmohsen.placesnearme.vm.AppViewModelFactory
import com.codewithmohsen.placesnearme.vm.DetailsViewModel
import com.codewithmohsen.placesnearme.vm.VenueViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(VenueViewModel::class)
    abstract fun bindVenueViewModel(venueViewModel: VenueViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(detailsViewModel: DetailsViewModel): ViewModel
}