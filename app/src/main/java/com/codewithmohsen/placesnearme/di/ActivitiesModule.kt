package com.codewithmohsen.placesnearme.di

import dagger.Module

@Module(includes = [MainActivityModule::class, DetailsActivityModule::class])
abstract class ActivitiesModule