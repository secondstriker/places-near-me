package com.codewithmohsen.placesnearme.di

import com.codewithmohsen.placesnearme.view.DetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class DetailsActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): DetailsActivity
}
