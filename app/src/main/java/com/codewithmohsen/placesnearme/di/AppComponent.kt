package com.codewithmohsen.placesnearme.di

import android.app.Application
import com.codewithmohsen.placesnearme.PlacesNearMeApp
import com.codewithmohsen.placesnearme.api.ApiService
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules =
        [AndroidInjectionModule::class,
            AppModule::class,
            ApiModule::class,
            ViewModelModule::class,
            ActivitiesModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(placesNearMeApp: PlacesNearMeApp)
}
