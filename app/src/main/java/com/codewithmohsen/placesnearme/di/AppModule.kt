package com.codewithmohsen.placesnearme.di

import android.app.Application
import android.telecom.Call
import androidx.room.Room
import com.codewithmohsen.placesnearme.AppConstants
import com.codewithmohsen.placesnearme.db.AppDb
import com.codewithmohsen.placesnearme.db.UserLocationDao
import com.codewithmohsen.placesnearme.db.VenueDao
import com.codewithmohsen.placesnearme.repository.DetailsRepository
import com.codewithmohsen.placesnearme.repository.DetailsRepositoryImpl
import com.codewithmohsen.placesnearme.repository.VenueRepository
import com.codewithmohsen.placesnearme.repository.VenueRepositoryImpl
import com.google.android.gms.location.LocationRequest
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDb {
        return Room
            .databaseBuilder(app, AppDb::class.java, "appdb.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideVenueDao(db: AppDb): VenueDao {
        return db.getVenueDao()
    }

    @Singleton
    @Provides
    fun provideUserLocationDao(db: AppDb): UserLocationDao {
        return db.getUserLocationDao()
    }

    @Singleton
    @Provides
    fun provideVenueRepository(venueRepositoryImpl: VenueRepositoryImpl): VenueRepository {
        return venueRepositoryImpl
    }

    @Singleton
    @Provides
    fun provideDetailsRepository(detailsRepositoryImpl: DetailsRepositoryImpl): DetailsRepository {
        return detailsRepositoryImpl
    }

    @Provides
    @Singleton
    fun provideLocationRequest(): LocationRequest {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = AppConstants.LOCATION_INTERVAL
        mLocationRequest.fastestInterval = AppConstants.LOCATION_FASTESTINTERVAL
        return mLocationRequest
    }
}