package com.codewithmohsen.placesnearme.binding

import android.app.Activity
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.codewithmohsen.placesnearme.R

/**
 * Binding adapters that work with an activity instance.
 */
class ImageBindingAdapters(private val activity: Activity, private val placeHolderResourceId: Int) {
    @BindingAdapter(value = ["imageUrl"], requireAll = false)
    fun bindImage(imageView: ImageView, url: String?) {
        Glide.with(activity).load(url).
        placeholder(placeHolderResourceId)
            .into(imageView)
    }
}

