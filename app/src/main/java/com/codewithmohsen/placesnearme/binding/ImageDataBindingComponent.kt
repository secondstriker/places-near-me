package com.codewithmohsen.placesnearme.binding

import android.app.Activity
import androidx.databinding.DataBindingComponent
import com.codewithmohsen.placesnearme.binding.ImageBindingAdapters

/**
 * A Data Binding Component implementation for activities.
 */
class ImageDataBindingComponent(activity: Activity,
                                private val placeHolderResourceId: Int): DataBindingComponent {
    private val adapter = ImageBindingAdapters(activity, placeHolderResourceId)
    override fun getImageBindingAdapters() = adapter
}
