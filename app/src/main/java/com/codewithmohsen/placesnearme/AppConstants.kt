package com.codewithmohsen.placesnearme

object AppConstants {

    //api params
    const val BASE_URL = "https://api.foursquare.com/v2/"
    const val CLIENT_ID = "XSZ4CR24MLSOF40RVLCVGHJMD2TZOWCFN0EFXIQFAW0XCOHA"
    const val CLIENT_SECRET = "BUE2PEKWYFRMWWQ3DFHCLJ0LEOHIVVBXDLH4QND4JR41DLZ2"
    const val VERSION = "20210416"

    //each request has maximum this offset number of venue in it
    const val OFFSET = 10

    //to figure out the data is old or not (in days)
    const val MAX_ACCEPTABLE_DURATION = 7L

    //to figure out the user is near to previous location or not (in meters)
    const val MAX_DISTANCE = 100

    const val LOCATION_INTERVAL:Long = 1000 * 45 //set the interval => location
    const val LOCATION_FASTESTINTERVAL:Long = 1000 * 30 //if a location is available sooner you can get it
}
