package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Group(
    @SerializedName("type")
    var type: String?,
    @SerializedName("count")
    var count: Int?,
    @SerializedName("items")
    var items: List<Item>?
)