package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class GroupXX(
    @SerializedName("type")
    var type: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("summary")
    var summary: String?,
    @SerializedName("count")
    var count: Int?,
    @SerializedName("items")
    var items: List<ItemXXX>?
)