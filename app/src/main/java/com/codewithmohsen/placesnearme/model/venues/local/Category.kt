package com.codewithmohsen.placesnearme.model.venues.local


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "category_table")
data class Category(

    @PrimaryKey(autoGenerate = true)
    @Expose
    @SerializedName("primaryId")
    var primaryId: Int = 0,

    @SerializedName("id")
    @ColumnInfo(name = "category_id")
    var categoryId: String = "",

    @ColumnInfo(name = "name")
    @SerializedName("name")
    var name: String? = null,

    @ColumnInfo(name = "plural_name")
    @SerializedName("pluralName")
    var pluralName: String? = null,

    @ColumnInfo(name = "short_name")
    @SerializedName("shortName")
    var shortName: String? = null,

    @Embedded
    @SerializedName("icon")
    var icon: Icon? = null,

    @ColumnInfo(name = "primary")
    @SerializedName("primary")
    var primary: Boolean? = null,

    @Expose
    @ColumnInfo(name = "related_venue_id")
    var relatedVenueId: String? = null
): Parcelable