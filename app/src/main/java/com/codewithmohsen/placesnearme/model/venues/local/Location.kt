package com.codewithmohsen.placesnearme.model.venues.local


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.codewithmohsen.placesnearme.model.venues.LabeledLatLng
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(

    @ColumnInfo(name = "address")
    @SerializedName("address")
    var address: String? = null,

    @Ignore
    @SerializedName("crossStreet")
    var crossStreet: String? = null,

    @ColumnInfo(name = "lat")
    @SerializedName("lat")
    var lat: Double = 0.0,

    @ColumnInfo(name = "lng")
    @SerializedName("lng")
    var lng: Double = 0.0,

    @Ignore
    @SerializedName("labeledLatLngs")
    var labeledLatLngs: List<LabeledLatLng>? = null,

    @ColumnInfo(name = "distance")
    @SerializedName("distance")
    var distance: Int = 0,

    @Ignore
    @SerializedName("cc")
    var cc: String? = null,

    @Ignore
    @SerializedName("city")
    var city: String? = null,

    @Ignore
    @SerializedName("state")
    var state: String? = null,

    @Ignore
    @SerializedName("country")
    var country: String? = null,

    @Ignore
    @SerializedName("formattedAddress")
    var formattedAddress: List<String>? = null,

    @ColumnInfo(name = "neighborhood")
    @SerializedName("neighborhood")
    var neighborhood: String? = null
): Parcelable