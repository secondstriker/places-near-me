package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Listed(
    @SerializedName("count")
    var count: Int?,
    @SerializedName("groups")
    var groups: List<GroupX>?
)