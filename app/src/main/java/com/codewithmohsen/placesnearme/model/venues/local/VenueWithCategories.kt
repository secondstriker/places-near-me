package com.codewithmohsen.placesnearme.model.venues.local

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import kotlinx.android.parcel.Parcelize

@Parcelize
class VenueWithCategories(
    @Embedded
    var venue: Venue,
    @Relation(
        parentColumn = "venue_id", entityColumn = "related_venue_id",  entity = Category::class)
    var categories: List<Category>? = null
): Parcelable
