package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Contact(
    @SerializedName("phone")
    var phone: String?,
    @SerializedName("formattedPhone")
    var formattedPhone: String?,
    @SerializedName("instagram")
    var instagram: String?
)