package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class DetailsResponse(
    @SerializedName("venue")
    var venueDetails: VenueDetails?
)