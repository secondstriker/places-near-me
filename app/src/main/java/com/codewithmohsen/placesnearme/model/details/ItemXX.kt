package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class ItemXX(
    @SerializedName("id")
    var id: String?,
    @SerializedName("createdAt")
    var createdAt: Int?
)