package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Likes(
    @SerializedName("count")
    var count: Int?,
    @SerializedName("groups")
    var groups: List<Group>?,
    @SerializedName("summary")
    var summary: String?
)