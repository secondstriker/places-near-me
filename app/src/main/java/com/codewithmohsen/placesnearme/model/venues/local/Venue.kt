package com.codewithmohsen.placesnearme.model.venues.local


import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "venue_table")
data class Venue(

    @Expose
    @PrimaryKey(autoGenerate = true)
    @SerializedName("primaryId")
    var primaryId: Int = 0,

    @ColumnInfo(name = "venue_id")
    @SerializedName("id")
    var venueId: String = "",

    @ColumnInfo(name = "name")
    @SerializedName("name")
    var name: String = "",

    @Embedded(prefix = "loc_")
    @SerializedName("location")
    var location: Location? = null,

    //we have relation for categories
    @Ignore
    @SerializedName("categories")
    var categories: List<Category>? = null
): Parcelable