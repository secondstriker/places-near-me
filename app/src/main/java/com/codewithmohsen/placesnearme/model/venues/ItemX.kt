package com.codewithmohsen.placesnearme.model.venues


import com.google.gson.annotations.SerializedName

data class ItemX(
    @SerializedName("summary")
    val summary: String?,
    @SerializedName("type")
    val type: String?,
    @SerializedName("reasonName")
    val reasonName: String?
)