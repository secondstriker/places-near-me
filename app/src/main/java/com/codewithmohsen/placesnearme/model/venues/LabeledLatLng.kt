package com.codewithmohsen.placesnearme.model.venues


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LabeledLatLng(
    @SerializedName("label")
    val label: String?,
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lng")
    val lng: Double?
): Parcelable