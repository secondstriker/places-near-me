package com.codewithmohsen.placesnearme.model.venues.local


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Icon(
    @SerializedName("prefix")
    var prefix: String? = null,
    @SerializedName("suffix")
    var suffix: String? = null
): Parcelable