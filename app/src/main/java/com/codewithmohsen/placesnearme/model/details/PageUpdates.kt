package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class PageUpdates(
    @SerializedName("count")
    var count: Int?,
    @SerializedName("items")
    var items: List<Any>?
)