package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Followers(
    @SerializedName("count")
    var count: Int?
)