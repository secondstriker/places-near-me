package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class LabeledLatLng(
    @SerializedName("label")
    var label: String?,
    @SerializedName("lat")
    var lat: Double?,
    @SerializedName("lng")
    var lng: Double?
)