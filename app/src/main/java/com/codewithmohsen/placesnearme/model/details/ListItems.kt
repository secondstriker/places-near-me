package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class ListItems(
    @SerializedName("count")
    var count: Int?,
    @SerializedName("items")
    var items: List<ItemXX>?
)