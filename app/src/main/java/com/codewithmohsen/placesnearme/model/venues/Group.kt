package com.codewithmohsen.placesnearme.model.venues


import com.google.gson.annotations.SerializedName

data class Group(
    @SerializedName("type")
    val type: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("items")
    val items: List<Item>?
)