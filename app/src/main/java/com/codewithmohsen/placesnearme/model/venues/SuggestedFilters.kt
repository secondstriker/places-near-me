package com.codewithmohsen.placesnearme.model.venues


import com.google.gson.annotations.SerializedName

data class SuggestedFilters(
    @SerializedName("header")
    val header: String?,
    @SerializedName("filters")
    val filters: List<Filter>?
)