package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Location(
    @SerializedName("address")
    var address: String?,
    @SerializedName("lat")
    var lat: Double?,
    @SerializedName("lng")
    var lng: Double?,
    @SerializedName("labeledLatLngs")
    var labeledLatLngs: List<LabeledLatLng>?,
    @SerializedName("distance")
    var distance: Int?,
    @SerializedName("cc")
    var cc: String?,
    @SerializedName("neighborhood")
    var neighborhood: String?,
    @SerializedName("city")
    var city: String?,
    @SerializedName("state")
    var state: String?,
    @SerializedName("country")
    var country: String?,
    @SerializedName("formattedAddress")
    var formattedAddress: List<String>?
)