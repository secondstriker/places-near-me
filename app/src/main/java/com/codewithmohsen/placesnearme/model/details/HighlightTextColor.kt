package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class HighlightTextColor(
    @SerializedName("photoId")
    var photoId: String?,
    @SerializedName("value")
    var value: Int?
)