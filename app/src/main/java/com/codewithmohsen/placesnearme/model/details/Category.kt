package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("id")
    var id: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("pluralName")
    var pluralName: String?,
    @SerializedName("shortName")
    var shortName: String?,
    @SerializedName("icon")
    var icon: Icon?,
    @SerializedName("primary")
    var primary: Boolean?
)