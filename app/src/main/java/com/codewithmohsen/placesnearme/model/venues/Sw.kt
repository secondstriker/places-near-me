package com.codewithmohsen.placesnearme.model.venues


import com.google.gson.annotations.SerializedName

data class Sw(
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lng")
    val lng: Double?
)