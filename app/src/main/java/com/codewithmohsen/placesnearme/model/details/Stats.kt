package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Stats(
    @SerializedName("tipCount")
    var tipCount: Int?
)