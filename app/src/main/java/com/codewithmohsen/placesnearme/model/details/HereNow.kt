package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class HereNow(
    @SerializedName("count")
    var count: Int?,
    @SerializedName("summary")
    var summary: String?,
    @SerializedName("groups")
    var groups: List<Any>?
)