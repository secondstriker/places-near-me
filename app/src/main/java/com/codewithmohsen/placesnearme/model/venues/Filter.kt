package com.codewithmohsen.placesnearme.model.venues


import com.google.gson.annotations.SerializedName

data class Filter(
    @SerializedName("name")
    val name: String?,
    @SerializedName("key")
    val key: String?
)