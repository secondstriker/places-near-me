package com.codewithmohsen.placesnearme.model.venues.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_location_table")
data class UserLocation (
    //we just need the last location that we got from location service when we are fetching
    //data from network, then, we always compare user location and calculate distance
    //between with this location, and then, we figure out we need a new network fetch or not
    //the same with fetch date to figure out old data
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var time: Long = 0L
){
    @PrimaryKey
    var id: Int = 0

}