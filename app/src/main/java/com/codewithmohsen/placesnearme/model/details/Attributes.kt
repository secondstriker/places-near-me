package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Attributes(
    @SerializedName("groups")
    var groups: List<GroupXX>?
)