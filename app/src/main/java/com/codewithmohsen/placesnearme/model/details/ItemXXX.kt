package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class ItemXXX(
    @SerializedName("displayName")
    var displayName: String?,
    @SerializedName("displayValue")
    var displayValue: String?,
    @SerializedName("priceTier")
    var priceTier: Int?
)