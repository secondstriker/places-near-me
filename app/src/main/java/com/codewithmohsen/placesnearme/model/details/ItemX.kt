package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class ItemX(
    @SerializedName("id")
    var id: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("type")
    var type: String?,
    @SerializedName("user")
    var user: User?,
    @SerializedName("editable")
    var editable: Boolean?,
    @SerializedName("public")
    var `public`: Boolean?,
    @SerializedName("collaborative")
    var collaborative: Boolean?,
    @SerializedName("url")
    var url: String?,
    @SerializedName("canonicalUrl")
    var canonicalUrl: String?,
    @SerializedName("createdAt")
    var createdAt: Int?,
    @SerializedName("updatedAt")
    var updatedAt: Int?,
    @SerializedName("photo")
    var photo: Photo?,
    @SerializedName("followers")
    var followers: Followers?,
    @SerializedName("listItems")
    var listItems: ListItems?
)