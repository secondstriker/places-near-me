package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class VenueDetails(
    @SerializedName("id")
    var id: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("contact")
    var contact: Contact?,
    @SerializedName("location")
    var location: Location?,
    @SerializedName("canonicalUrl")
    var canonicalUrl: String?,
    @SerializedName("categories")
    var categories: List<Category>?,
    @SerializedName("stats")
    var stats: Stats?,
    @SerializedName("url")
    var url: String?,
    @SerializedName("likes")
    var likes: Likes?,
    @SerializedName("dislike")
    var dislike: Boolean?,
    @SerializedName("ok")
    var ok: Boolean?,
    @SerializedName("allowMenuUrlEdit")
    var allowMenuUrlEdit: Boolean?,
    @SerializedName("specials")
    var specials: Specials?,
    @SerializedName("reasons")
    var reasons: Reasons?,
    @SerializedName("hereNow")
    var hereNow: HereNow?,
    @SerializedName("createdAt")
    var createdAt: Int?,
    @SerializedName("shortUrl")
    var shortUrl: String?,
    @SerializedName("timeZone")
    var timeZone: String?,
    @SerializedName("listed")
    var listed: Listed?,
    @SerializedName("seasonalHours")
    var seasonalHours: List<Any>?,
    @SerializedName("pageUpdates")
    var pageUpdates: PageUpdates?,
    @SerializedName("inbox")
    var inbox: Inbox?,
    @SerializedName("attributes")
    var attributes: Attributes?,
    @SerializedName("bestPhoto")
    var bestPhoto: BestPhoto?,
    @SerializedName("colors")
    var colors: Colors?
)