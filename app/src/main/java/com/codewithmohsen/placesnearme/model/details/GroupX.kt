package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class GroupX(
    @SerializedName("type")
    var type: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("count")
    var count: Int?,
    @SerializedName("items")
    var items: List<ItemX>?
)