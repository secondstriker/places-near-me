package com.codewithmohsen.placesnearme.model.details


import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("firstName")
    var firstName: String?,
    @SerializedName("lastName")
    var lastName: String?,
    @SerializedName("countryCode")
    var countryCode: String?
)