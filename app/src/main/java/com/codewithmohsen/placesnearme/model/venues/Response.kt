package com.codewithmohsen.placesnearme.model.venues


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("suggestedFilters")
    val suggestedFilters: SuggestedFilters?,
    @SerializedName("suggestedRadius")
    val suggestedRadius: Int?,
    @SerializedName("headerLocation")
    val headerLocation: String?,
    @SerializedName("headerFullLocation")
    val headerFullLocation: String?,
    @SerializedName("headerLocationGranularity")
    val headerLocationGranularity: String?,
    @SerializedName("totalResults")
    val totalResults: Int?,
    @SerializedName("suggestedBounds")
    val suggestedBounds: SuggestedBounds?,
    @SerializedName("groups")
    val groups: List<Group>?
)