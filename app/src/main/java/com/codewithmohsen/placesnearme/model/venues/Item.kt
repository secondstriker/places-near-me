package com.codewithmohsen.placesnearme.model.venues


import com.codewithmohsen.placesnearme.model.venues.local.Venue
import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("reasons")
    val reasons: Reasons?,
    @SerializedName("venue")
    val venue: Venue?,
    @SerializedName("referralId")
    val referralId: String?
)