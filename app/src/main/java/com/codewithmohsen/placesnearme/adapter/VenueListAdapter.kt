package com.codewithmohsen.placesnearme.adapter

import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.codewithmohsen.placesnearme.AppCoroutineDispatchers
import com.codewithmohsen.placesnearme.R
import com.codewithmohsen.placesnearme.databinding.ItemVenueBinding
import com.codewithmohsen.placesnearme.model.venues.local.VenueWithCategories

/**
 * A RecyclerView adapter for [Item List] class.
 */
class VenueListAdapter(
    appCoroutineDispatchers: AppCoroutineDispatchers,
    private val dataBindingComponent: DataBindingComponent,
    private val itemClickCallback: ((VenueWithCategories) -> Unit)?
) : DataBoundListAdapter<VenueWithCategories, ItemVenueBinding>(
    appCoroutineDispatchers = appCoroutineDispatchers,
    diffCallback = object : DiffUtil.ItemCallback<VenueWithCategories>() {
        override fun areItemsTheSame(oldItem: VenueWithCategories, newItem: VenueWithCategories): Boolean {
            return oldItem.venue.venueId == newItem.venue.venueId
        }

        override fun areContentsTheSame(oldItem: VenueWithCategories, newItem: VenueWithCategories): Boolean {
            return oldItem.venue.primaryId == newItem.venue.primaryId &&
            oldItem.venue.venueId == newItem.venue.venueId &&
            oldItem.categories == newItem.categories &&
            oldItem.venue.name == newItem.venue.name &&
            oldItem.venue.location == newItem.venue.location
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ItemVenueBinding {

        val binding = DataBindingUtil.inflate<ItemVenueBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_venue,
            parent,
            false,
            dataBindingComponent)

        binding.root.setOnClickListener { _ ->
            binding.item.let {
                if(it != null) itemClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: ItemVenueBinding, item: VenueWithCategories) {
        binding.item = item
    }
}
