package com.codewithmohsen.placesnearme.view

import android.content.Intent
import android.location.Location
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.codewithmohsen.placesnearme.AppCoroutineDispatchers
import com.codewithmohsen.placesnearme.R
import com.codewithmohsen.placesnearme.adapter.VenueListAdapter
import com.codewithmohsen.placesnearme.binding.ImageDataBindingComponent
import com.codewithmohsen.placesnearme.databinding.ActivityMainBinding
import com.codewithmohsen.placesnearme.vm.VenueViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AbstractLocationActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appCoroutineDispatchers: AppCoroutineDispatchers

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: VenueListAdapter
    private var imageDataBindingComponent: DataBindingComponent =
        ImageDataBindingComponent(this, R.drawable.ic_not_listed_location_24)


    private val viewModel: VenueViewModel by viewModels {
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        adapter = VenueListAdapter(appCoroutineDispatchers, imageDataBindingComponent) { item ->

            val intent = Intent(this, DetailsActivity::class.java)
            intent.putExtra(VENUE_ID, item.venue.venueId)
            startActivity(intent)
        }
        binding.itemList.layoutManager = LinearLayoutManager(this)
        binding.itemList.adapter = adapter
        initItemList()

        binding.itemList.addOnScrollListener(object :
            EndlessRecyclerOnScrollListener(binding.itemList.layoutManager!!, 0) {
            override fun onLoadMore() {
                viewModel.getMoreVenues()
            }
        })

        setSwipeRefreshListener()
    }

    private fun setSwipeRefreshListener() {
        binding.swipeRefresh.setOnRefreshListener {
            super.onLastLocation {
                viewModel.setLl(it.latitude, it.longitude)
            }
        }
    }

    private fun initItemList() {
        viewModel.getVenues().observe(this, Observer { resource ->
            adapter.submitList(resource?.data)
            binding.status = resource?.status
            binding.swipeRefresh.isRefreshing = false
        })
    }

    override fun onGetLocation(location: Location) {
        viewModel.setLl(location.latitude, location.longitude)
    }

    companion object {
        const val VENUE_ID = "venue_id"
    }
}