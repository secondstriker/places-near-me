package com.codewithmohsen.placesnearme.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.codewithmohsen.placesnearme.AppCoroutineDispatchers
import com.codewithmohsen.placesnearme.R
import com.codewithmohsen.placesnearme.adapter.VenueListAdapter
import com.codewithmohsen.placesnearme.binding.ImageDataBindingComponent
import com.codewithmohsen.placesnearme.databinding.ActivityMainBinding
import com.codewithmohsen.placesnearme.utils.checkLocationPermission
import com.codewithmohsen.placesnearme.utils.isLocationEnabled
import com.codewithmohsen.placesnearme.vm.VenueViewModel
import com.google.android.gms.location.*
import dagger.android.AndroidInjection
import javax.inject.Inject

abstract class AbstractLocationActivity : AppCompatActivity() {

    @Inject
    lateinit var locationRequest: LocationRequest
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)


        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(applicationContext)
    }


    override fun onResume() {
        super.onResume()
        getLocationPermission {
            trackLocation()
        }
    }

    override fun onPause() {
        super.onPause()
        stopTrackLocation()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLocationPermission {
                    trackLocation()
                }
            }
        }
    }

    private fun getLocationPermission(success: (() -> Unit)) {
        if (applicationContext.isLocationEnabled() && applicationContext.checkLocationPermission()) {
            success.invoke()
        } else {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    PERMISSION_ID
                )
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun trackLocation() {

        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest, mLocationCallback,
            Looper.getMainLooper()
        )
    }

    private fun stopTrackLocation() {
        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
    }

    private var mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {

            if(locationResult?.lastLocation == null)
                return

            onGetLocation(locationResult.lastLocation)
        }
    }

    @SuppressLint("MissingPermission")
    protected fun onLastLocation(success: (location: Location) -> Unit) {
        getLocationPermission {
            fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                success.invoke(it)
            }
        }
    }

    abstract fun onGetLocation(location: Location)

    companion object {
        const val PERMISSION_ID = 110
    }
}