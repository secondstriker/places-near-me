package com.codewithmohsen.placesnearme.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.codewithmohsen.placesnearme.R
import com.codewithmohsen.placesnearme.binding.ImageDataBindingComponent
import com.codewithmohsen.placesnearme.databinding.ActivityDetailsBinding
import com.codewithmohsen.placesnearme.vm.DetailsViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject


class DetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: ActivityDetailsBinding

    private var imageDataBindingComponent: DataBindingComponent =
        ImageDataBindingComponent(this, R.drawable.ic_image_24)

    private val viewModel: DetailsViewModel by viewModels {
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        DataBindingUtil.setDefaultComponent(imageDataBindingComponent)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val venueId = intent.getStringExtra(MainActivity.VENUE_ID)
        check(venueId != null)

        viewModel.getDetails(venueId).observe(this, Observer { resource ->
            binding.item = resource.data
            binding.status = resource.status
        })
    }

    fun navigate(view: View) {
        val lat = binding.item?.location?.lat
        val lng = binding.item?.location?.lng
        if(lat == null || lng == null) return

        val gmmIntentUri = Uri.parse("google.navigation:q=$lat, $lng&mode=c")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }

    fun moreInformation(view: View){
        val url = binding.item?.canonicalUrl ?: return
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}