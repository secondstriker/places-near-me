package com.codewithmohsen.placesnearme.utils

import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.databinding.BindingAdapter
import com.codewithmohsen.placesnearme.model.venues.local.UserLocation
import com.google.android.material.textfield.TextInputLayout
import kotlin.math.*


val <T> T.exhaustive: T
    get() = this

fun List<Any>.toNextPage(offset: Int): Int =
    (ceil(this.count().toDouble() / offset) + 1).toInt()

fun Context.isLocationEnabled(): Boolean {
    var locationManager: LocationManager =
        getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
        LocationManager.NETWORK_PROVIDER
    )
}

fun Context.checkLocationPermission(): Boolean =
    this.checkCallingOrSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED



//this extension in written according to this
//https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude
/**
 * Calculate distance between two points in latitude and longitude
 *  Uses Haversine method as its base.
 * @returns Distance in Meters
 */
fun UserLocation.distance(userLocation: UserLocation): Double {
    val R = 6371 // Radius of the earth
    val latDistance = Math.toRadians(userLocation.latitude - this.latitude)
    val lonDistance = Math.toRadians(userLocation.longitude - this.longitude)
    val a = (sin(latDistance / 2) * sin(latDistance / 2)
            + (cos(Math.toRadians(this.latitude)) * cos(Math.toRadians(userLocation.latitude))
            * sin(lonDistance / 2) * sin(lonDistance / 2)))
    val c = 2 * atan2(sqrt(a), sqrt(1 - a))
    var distance = R * c * 1000 // convert to meters
    distance = distance.pow(2.0)
    return sqrt(distance)
}

/**
 * duration between two time
 * @param lastUserLocation: last userLocation obj
 * @returns time difference in days
 */
fun UserLocation.durationTimeWithLastSavedLocation(lastUserLocation: UserLocation): Long {

    val diff = this.time - lastUserLocation.time
    return (diff / (1000 * 3600 * 24))
}